# select 

#### 项目介绍
go 语言 context 跟select用法。

#### 软件架构

 ` 

    func SelectT() {
    
	ctx, cancel := context.WithCancel(context.Background())

	select {
	case g := <-func(ctx2 context.Context) <-chan int {
		result := make(chan int, 1)

		go func(ctx3 context.Context) {
			fmt.Println("in")
			c := make(chan int)
			go func() {
				fmt.Println("in C!")
				time.Sleep(2 * time.Second)
				c <- 2
			}()
			select {
			case <-ctx.Done():
				fmt.Println("in Done!")
				runtime.Goexit()
				return
			case <-c:
				fmt.Println("in result!")
				result <- 80
			}

			fmt.Println("in after!")

		}(ctx)
		fmt.Println("-ch0")
		return result
	}(ctx):
		fmt.Println("out g :", g)
		return
	case <-time.After(4 * time.Second):
		fmt.Println("-ch1")
		cancel()
	case <-func() <-chan int {
		ch2 := make(chan int, 1)
		fmt.Println("-ch2")
		return ch2
	}():
	case <-func() <-chan int {
		ch3 := make(chan int, 1)
		fmt.Println("-ch3")
		return ch3
	}():
	case <-func() <-chan int {
		ch4 := make(chan int, 1)
		fmt.Println("-ch4")
		return ch4
	}():
	}
    }

`

#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)